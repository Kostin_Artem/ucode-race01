.OBJECTS = $(wildcard src/*.c)
OBJECTS = $(.OBJECTS:src%.c=obj%.o)

all: prepare part_of_the_matrix

prepare:
	mkdir -p obj/

part_of_the_matrix: $(OBJECTS)
	clang -std=c11 -Wall -Wextra -Werror -Wpedantic -Iinc -o $@ $^

obj/%.o: src/%.c
	clang -std=c11 -Wall -Wextra -Werror -Wpedantic -Iinc -o $@ -c $<

clean:
	rm -rf obj
	rm -rf src
	rm -rf inc

uninstall:
	rm -rf obj
	rm -f part_of_the_matrix

reinstall: uninstall all
