#include "part_of_the_matrix.h"

static bool is_operator(char c) {
    return c == '+' || c == '-' || c == '*' || c == '/' || c == '?';
}

// Returns '+' '-' '/' '*' '?'.
char mx_parse_operator(const char *arg) {
    const char *begin = arg;
    char operator = 0;

    while (mx_isspace(*arg)) {
        ++arg;
    }
    
    operator = *arg++;
    if (!is_operator(operator)) {
        mx_exit_invalid("operation", begin);
    }

    while (mx_isspace(*arg)) {
        ++arg;
    }
    if (*arg != '\0') {
        mx_exit_invalid("operation", begin);
    }

    return operator;
}
