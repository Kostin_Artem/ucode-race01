#include <stdbool.h>

bool mx_isdigit(char c) {
    if (c >= '0' && c <= '9') {
        return true;
    }

    return false;
}

bool mx_is_valid_digit(char c) {
    return c == '?' || mx_isdigit(c);
}

bool mx_isspace(char c) {
    /*
     * See "man isspace".
     */
    if (c == ' '
        || c == '\t'
        || c == '\n'
        || c == '\v'
        || c == '\f'
        || c == '\r') {
        return true;
    }

    return false;
}
