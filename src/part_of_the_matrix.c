#include "part_of_the_matrix.h"

static void parse_args(char** argv, char *op,
                       t_number_info *nums) {
    *op = mx_parse_operator(argv[2]);
    mx_init_number_info(nums, argv[1]);
    mx_init_number_info(nums + 1, argv[3]);
    mx_init_number_info(nums + 2, argv[4]);
    if (!mx_parse_number(nums)) {
        mx_exit_invalid("operand", argv[1]);
    }
    if (!mx_parse_number(nums + 1)) {
        mx_free_numbers(nums);
        mx_exit_invalid("operand", argv[3]);
    }
    if (!mx_parse_number(nums + 2)) {
        mx_free_numbers(nums);
        mx_exit_invalid("result", argv[4]);
    }
}

static void handle_operation(char op, t_number_info *nums) {
    if (op == '+')
        mx_handle_add(nums);
    else if (op == '-')
        mx_handle_sub(nums);
    else if (op == '*')
        mx_handle_mul(nums);
    else if (op == '/')
        mx_handle_div(nums);
    else {
        mx_handle_add(nums);
        mx_handle_sub(nums);
        mx_handle_mul(nums);
        mx_handle_div(nums);
    }
}

int main(int argc, char **argv) {
    char op;
    t_number_info nums[3];
    if (argc != 5)
        mx_exit_usage();
    parse_args(argv, &op, nums);
    handle_operation(op, nums);
    mx_free_numbers(nums);

    return 0;
}
