#include "part_of_the_matrix.h"
#include <stdlib.h>
#include <limits.h>

// Checks number structure and gets sign.
static bool check_structure(const char *arg, int *sign) {
    while (mx_isspace(*arg))
        ++arg;
    if (*arg == '-' || *arg == '+') {
        if (*arg == '-')
            *sign = -1;
        ++arg;
    }
    if (!mx_is_valid_digit(*arg))
        return false;
    while (mx_is_valid_digit(*arg))
        ++arg;
    while (mx_isspace(*arg))
        ++arg;
    if (*arg != '\0')
        return false;
    return true;
}

// Checks digits count. Cuts leading ? if needed.
// Counts '?'.
static bool check_digits_count(t_number_info *info, char **end) {
    char *it = *end;
    int d_count = 0;
    while (mx_is_valid_digit(*it)) {
        if (*it == '?')
            ++info->q_count;
        ++it;
        ++d_count;
    }
    while (d_count > MAX_DIGITS) {
        if (mx_isdigit(*info->number))
            return false;
        --d_count;
        --info->q_count;
        ++info->number;
    }
    *end = it;
    return true;
}

// Checks overflow of the base.
static bool check_overflows(t_number_info *info) {
    long long base = 0;
    for (int i = 0; mx_is_valid_digit(info->number[i]); ++i) {
        base *= 10;
        if (info->number[i] != '?')
            base += info->number[i] - '0';
        if ((info->sign == 1 && base > INT_MAX)
            || (info->sign == -1 && base > INT_MAX + 1LL))
            return false;
    }

    return true;
}

// Removes all symbols except of digits and '?' from the string.
// Removes leading '?' if d_count > 10. Else returns false.
// Counts '?'.
static bool strip_number(t_number_info *info) {
    char *it = info->number;
    while (!mx_is_valid_digit(*it))
        ++it;
    info->number = it;
    if (!check_digits_count(info, &it))
        return false;
    if (!check_overflows(info))
        return false;
    *it = '\0';
    return true;
}

bool mx_parse_number(t_number_info *info) {
    if (!check_structure(info->number, &info->sign))
        return false;
    if (!strip_number(info))
        return false;
    if (info->q_count == 0)
        return true;
    info->q_arr = malloc(info->q_count * sizeof(char *));
    if (info->q_arr == NULL)
        exit(-1);
    {
        int arr_it = 0;
        for (int i = mx_strlen(info->number) - 1; i >= 0; --i) {
            if (info->number[i] == '?')
                info->q_arr[arr_it++] = info->number + i;
        }
    }
    return true;
}
