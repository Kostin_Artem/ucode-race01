#include "part_of_the_matrix.h"
#include <unistd.h>
#include <stdlib.h>

static void printerr(const char *s) {
    write(STDERR_FILENO, s, mx_strlen(s));
}

void mx_exit_usage(void) {
    printerr(ERROR_USAGE_1);
    printerr(ERROR_USAGE_2);
    exit(0);
}

void mx_exit_invalid(const char *type, const char *arg) {
    printerr("Invalid ");
    printerr(type);
    printerr(": ");
    printerr(arg);
    printerr("\n");
    exit(0);
}

void mx_printstr(const char *s) {
    write(STDOUT_FILENO, s, mx_strlen(s));
}

void mx_print_operation(t_number_info *num1, t_number_info *num2,
                        t_number_info *res, const char *op) {
    mx_printinta(num1);
    mx_printstr(op);
    mx_printinta(num2);
    mx_printstr(" = ");
    mx_printinta(res);
    mx_printstr("\n");
}
