#include <stdbool.h>

// Only for the positive power.
int mx_pow(int num, int pow) {
    int res = num;
    if (pow == 0)
        return 1;
    for (int i = 1; i < pow; ++i)
        res *= num;
    return res;
}
