#include "part_of_the_matrix.h"
#include <stdlib.h>

void mx_init_number_info(t_number_info *info, char *arg) {
    info->number = arg;
    info->sign = 1;
    info->q_arr = NULL;
    info->q_count = 0;
}

void mx_free_numbers(t_number_info *nums) {
    for (int i = 0; i < 3; ++i) {
        if (nums[i].q_arr != NULL) {
            free(nums[i].q_arr);
            nums[i].q_arr = NULL;
        }
    }
}
