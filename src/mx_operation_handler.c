#include "part_of_the_matrix.h"

static bool form_number(t_number_info *info, int counter, int *num) {
    for (int i = 0; i < info->q_count; ++i) {
        *info->q_arr[i] = '0' + counter % 10;
        counter /= 10;
    }
    return mx_atoi(info->number, info->sign, num);
}

void mx_handle_add(t_number_info *nums) {
    int limits[3];
    int numbers[3];
    limits[0] = mx_pow(10, nums[0].q_count);
    limits[1] = mx_pow(10, nums[1].q_count);
    limits[2] = mx_pow(10, nums[2].q_count);
    for (int count1 = 0; count1 < limits[0]; ++count1) {
        if (!form_number(nums, count1, numbers))
            break;
        for (int count2 = 0; count2 < limits[1]; ++count2) {
            if (!form_number(nums + 1, count2, numbers + 1))
                break;
            for (int count3 = 0; count3 < limits[2]; ++count3) {
                if (!form_number(nums + 2, count3, numbers + 2))
                    break;
                if (numbers[0] + numbers[1] == numbers[2])
                    mx_print_operation(nums, nums + 1, nums + 2, " + ");
            }
        }
    }
}

void mx_handle_sub(t_number_info *nums) {
    int limits[3];
    int numbers[3];
    limits[0] = mx_pow(10, nums[0].q_count);
    limits[1] = mx_pow(10, nums[1].q_count);
    limits[2] = mx_pow(10, nums[2].q_count);
    for (int count1 = 0; count1 < limits[0]; ++count1) {
        if (!form_number(nums, count1, numbers))
            break;
        for (int count2 = 0; count2 < limits[1]; ++count2) {
            if (!form_number(nums + 1, count2, numbers + 1))
                break;
            for (int count3 = 0; count3 < limits[2]; ++count3) {
                if (!form_number(nums + 2, count3, numbers + 2))
                    break;
                if (numbers[0] - numbers[1] == numbers[2])
                    mx_print_operation(nums, nums + 1, nums + 2, " - ");
            }
        }
    }
}

void mx_handle_mul(t_number_info *nums) {
    int limits[3];
    int numbers[3];
    limits[0] = mx_pow(10, nums[0].q_count);
    limits[1] = mx_pow(10, nums[1].q_count);
    limits[2] = mx_pow(10, nums[2].q_count);
    for (int count1 = 0; count1 < limits[0]; ++count1) {
        if (!form_number(nums, count1, numbers))
            break;
        for (int count2 = 0; count2 < limits[1]; ++count2) {
            if (!form_number(nums + 1, count2, numbers + 1))
                break;
            for (int count3 = 0; count3 < limits[2]; ++count3) {
                if (!form_number(nums + 2, count3, numbers + 2))
                    break;
                if (numbers[0] * numbers[1] == numbers[2])
                    mx_print_operation(nums, nums + 1, nums + 2, " * ");
            }
        }
    }
}

void mx_handle_div(t_number_info *nums) {
    int limits[3];
    int numbs[3];
    limits[0] = mx_pow(10, nums[0].q_count);
    limits[1] = mx_pow(10, nums[1].q_count);
    limits[2] = mx_pow(10, nums[2].q_count);
    for (int count1 = 0; count1 < limits[0]; ++count1) {
        if (!form_number(nums, count1, numbs))
            break;
        for (int count2 = 0; count2 < limits[1]; ++count2) {
            if (!form_number(nums + 1, count2, numbs + 1)) break;
            if (numbs[1] == 0)
                continue;
            for (int count3 = 0; count3 < limits[2]; ++count3) {
                if (!form_number(nums + 2, count3, numbs + 2))
                    break;
                if (numbs[0] / numbs[1] == numbs[2])
                    mx_print_operation(nums, nums + 1, nums + 2, " / ");
            }
        }
    }
}
