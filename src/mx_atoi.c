#include "part_of_the_matrix.h"
#include <limits.h>

bool mx_atoi(const char *str, int sign, int *num) {
    long long value = 0;

    while (mx_isdigit(*str)) {
        value *= 10;
        value += *str - '0';
        if ((sign > 0 && value > INT_MAX)
            || (sign < 0 && value > INT_MAX + 1LL)) {
            return false;
        }
        ++str;
    }

    *num = value * sign;
    return true;
}

void mx_printinta(t_number_info *num) {
    char *num_it = num->number;
    while (*num_it == '0' && *(num_it + 1) != '\0')
        ++num_it; 
    if (num->sign == -1 && *num_it != '0')
        mx_printstr("-");
    mx_printstr(num_it);
}
