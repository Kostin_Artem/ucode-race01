#pragma once
#include <stdbool.h>

#define ERROR_USAGE_1 "./part_of_the_matrix "
#define ERROR_USAGE_2 "[operand1] [operation] [operand2] [result]\n"
#define MAX_DIGITS 10

typedef struct s_number_info {
    char *number;
    char **q_arr;
    int q_count;
    int sign;
}              t_number_info;

void mx_init_number_info(t_number_info *info, char *arg);
void mx_free_numbers(t_number_info *nums);

int mx_strlen(const char *s);
void mx_exit_usage(void);
void mx_exit_invalid(const char *type, const char *arg);
void mx_printstr(const char *s);
void mx_print_operation(t_number_info *num1, t_number_info *num2,
                        t_number_info *res, const char *op);

bool mx_isdigit(char c);
bool mx_isspace(char c);
bool mx_is_valid_digit(char c);

int mx_pow(int num, int pow);

char mx_parse_operator(const char *arg);
bool mx_parse_number(t_number_info *info);

bool mx_atoi(const char *str, int sign, int *num);
void mx_printinta(t_number_info *num);

void mx_handle_add(t_number_info *nums);
void mx_handle_sub(t_number_info *nums);
void mx_handle_mul(t_number_info *nums);
void mx_handle_div(t_number_info *nums);
